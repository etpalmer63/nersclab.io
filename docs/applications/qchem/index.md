# Q-Chem

[Q-Chem](http://www.q-chem.com/) is a comprehensive ab initio quantum
chemistry package for accurate predictions of molecular structures,
reactivities, and vibrational, electronic and NMR spectra. The new
release of Q-Chem 6 represents the state-of-the-art of methodology
from the highest performance DFT/HF calculations to high level post-HF
correlation methods:

* Fully integrated graphic interface including molecular builder,
  input generator, contextual help and visualization toolkit (See
  amazing image below generated with IQmol; multiple copies available
  free of charge);
* Dispersion-corrected and double hybrid DFT functionals;
* Faster algorithms for DFT, HF, and coupled-cluster calculations;
* Structures and vibrations of excited states with TD-DFT;
* Methods for mapping complicated potential energy surfaces;
* Efficient valence space models for strong correlation;
* More choices for excited states, solvation, and charge-transfer;
* Effective Fragment Potential and QM/MM for large systems;
* For a complete list of new features, see the
  [Q-Chem support page](https://www.q-chem.com/support/).

## Accessing Q-Chem

Q-Chem is available to all NERSC users as a module. To load
Q-Chem into your environment, type:

```bash
module load qchem
```

## Application Information, Documentation, and Support

[Q-Chem User's manual](https://manual.q-chem.com/latest/)

## Related Applications

* [NWChem](../nwchem/index.md)

## User Contributed Information

!!! info "Please help us improve this page"
	Users are invited to contribute helpful information and corrections
	through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
