# NERSC Tutorials

## Modules

- [Lmod Training](https://gitlab.com/NERSC/lmod-training)

## Spack

- [Spack Nightly Build Pipeline](https://software.nersc.gov/ci-resources/spack-nightly-build)
- [Spack CI Pipleline](https://software.nersc.gov/ci-resources/spack-ci-pipelines)

## Containers

- [Podman-hpc for beginners](../development/containers/podman-hpc/podman-beginner-tutorial.md)
- [Shifter for beginners](../development/containers/shifter/shifter-beginner-tutorial.md)

## Quantum Computing

- CUDA Quantum, a high-performance platform for hybrid quantum-classical computing:
    1. [Instructions to run CUDA Quantum container on Perlmutter](https://github.com/poojarao8/nersc-quantum-day/blob/master/PerlmutterInstructions.md#steps-to-run-with-the-cuda-quantum-container-on-perlmutter)
    2. [Jupyter notebook covering the basics of CUDA Quantum](https://github.com/poojarao8/nersc-quantum-day/blob/master/cudaq_basics.ipynb)
