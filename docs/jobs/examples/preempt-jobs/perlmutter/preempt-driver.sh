#!/bin/bash
#SBATCH -q preempt
#SBATCH -C cpu
#SBATCH -N 1
#SBATCH --time=24:00:00
#SBATCH --signal=USR1@60 
#SBATCH --requeue 
#SBATCH --open-mode=append
srun variable-time-payload.sh
sleep 120 # make sure a process is still running for slurm to send SIGKILL to
