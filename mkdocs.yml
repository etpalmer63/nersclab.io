nav:
  - Home: index.md
  - Getting Started at NERSC: getting-started.md
  - Understanding Our Systems:
    - systems/index.md
    - Perlmutter:
      - Architecture: systems/perlmutter/architecture.md
      - Timeline: systems/perlmutter/timeline/index.md
      - Current Known Issues: current.md
  - Getting and Managing Your Account:
    - accounts/index.md
    - Passwords: accounts/passwords.md
    - Policy: accounts/policy.md
    - Collaboration Accounts: accounts/collaboration_accounts.md
    - Iris Guide for Users: iris/iris-for-users.md
    - Associating Publications with Your Account: iris/iris-publications.md
  - Connecting to NERSC:
    - Establishing Connections: connect/index.md
    - Multi-Factor Authentication: connect/mfa.md
    - Federated Identity: connect/federatedid.md
    - NoMachine / NX, X Windows Accelerator: connect/nx.md
    - Visual Studio Code / VSCode: connect/vscode.md
  - Working in a NERSC Shell: 
    - Customizing and Troubleshooting Shells: environment/index.md
    - Shell Startup: environment/shell_startup.md
    - Lmod: environment/lmod.md
    - Containers:
      - development/containers/index.md
      - Podman-hpc:
        - Podman-hpc for Beginners Tutorial: development/containers/podman-hpc/podman-beginner-tutorial.md
        - Using Podman-hpc at NERSC: development/containers/podman-hpc/overview.md
      - Shifter:
        - development/containers/shifter/index.md
        - Shifter for Beginners Tutorial: development/containers/shifter/shifter-beginner-tutorial.md
        - How to use Shifter: development/containers/shifter/how-to-use.md
        - FAQ and Troubleshooting: development/containers/shifter/faq-troubleshooting.md
        - Example Shifter Containers: development/containers/shifter/examples.md
      - registry.nersc.gov:
            - development/containers/registry/index.md
  - Running Jobs:
    - Basics of Running Jobs: jobs/index.md
    - Running Jobs on Perlmutter: systems/perlmutter/running-jobs/index.md
    - Resource Usage Policies: policies/resource-usage.md
    - Scheduling: jobs/scheduling.md
    - Queues and Charges: jobs/policy.md
    - Interactive: jobs/interactive.md
    - Example Jobs: jobs/examples/index.md
    - Best Practices: jobs/best-practices.md
    - Jobscript Generator: jobs/jobscript-generator.md
    - Troubleshooting Jobs: jobs/troubleshooting.md
    - Monitoring: jobs/monitoring.md
    - Affinity: jobs/affinity/index.md
    - Reservations: jobs/reservations.md
    - Checkpoint/Restart:
      - Checkpoint/Restart Overview: development/checkpoint-restart/index.md
      - DMTCP: development/checkpoint-restart/dmtcp/index.md
      - MANA: development/checkpoint-restart/mana/index.md
  - Running Workflows:
    - Workflow Tools: jobs/workflow/index.md
    - Workflow queue: jobs/workflow/workflow-queue.md
    - Scrontab: jobs/workflow/scrontab.md
    - GNU Parallel: jobs/workflow/gnuparallel.md
    - TaskFarmer: jobs/workflow/taskfarmer.md
    - FireWorks: jobs/workflow/fireworks.md
    - Nextflow: jobs/workflow/nextflow.md
    - Papermill: jobs/workflow/papermill.md
    - Parsl: jobs/workflow/parsl.md
    - Snakemake: jobs/workflow/snakemake.md
    - Community Supported Tools: jobs/workflow/community_tools.md
  - Using Applications and Frameworks: 
    - Applications Overview: applications/index.md
    - Supported Applications and Frameworks:
      - AMBER: applications/amber/index.md
      - BerkeleyGW: applications/berkeleygw/index.md
      - CP2K: applications/cp2k/index.md
      - Dask: analytics/dask.md
      - E4S:
        - applications/e4s/index.md
        - Spack v0.22: applications/e4s/perlmutter/spack-v0.22.md
        - 23.08: applications/e4s/perlmutter/23.08.md
        - 23.05: applications/e4s/perlmutter/23.05.md
        - 22.11: applications/e4s/perlmutter/22.11.md
        - Spack-Gitlab-Pipeline: applications/e4s/spack_gitlab_pipeline.md
        - Spack Environments: applications/e4s/spack_environments.md
      - Gromacs: applications/gromacs/index.md
      - Jupyter:
        - Jupyter Overview: services/jupyter/index.md
        - How-To Guides: services/jupyter/how-to-guides.md
        - Reference: services/jupyter/reference.md
        - Background: services/jupyter/background.md
      - LAMMPS: applications/lammps/index.md
      - Mathematica: applications/mathematica/index.md
      - MATLAB:
        - MATLAB: applications/matlab/index.md
        - MATLAB Compiler: applications/matlab/matlab_compiler.md
      - NAMD: applications/namd/index.md
      - NCL: applications/ncl/index.md
      - NWChem: applications/nwchem/index.md
      - ORCA: applications/orca/index.md
      - ParaView: applications/paraview/index.md
      - PyTorch: machinelearning/pytorch.md
      - Q-Chem: applications/qchem/index.md
      - Quantum ESPRESSO: applications/quantum-espresso/index.md
      - ROOT: development/languages/cernroot.md
      - Spark: analytics/spark.md
      - TensorBoard: machinelearning/tensorboard.md
      - TensorFlow: machinelearning/tensorflow.md
      - VASP: applications/vasp/index.md
      - Wannier90: applications/wannier90/index.md
      - WRF:
        - Introduction to WRF: applications/wrf/index.md
        - WPS: applications/wrf/wps.md
        - WRF: applications/wrf/wrf.md
        - WRF module: applications/wrf/wrf_module.md
        - WRF benchmark: applications/wrf/wrf_benchmark.md
        - Publications: applications/wrf/publications.md
    - Analytics: analytics/analytics.md #needs update and links to apps
    - Machine Learning: 
      - machinelearning/index.md
      - Distributed training: machinelearning/distributed-training/index.md
      - Hyperparameter optimization: machinelearning/hpo.md
      - Science use-cases: machinelearning/science-use-cases/index.md
      - Deep Networks for HEP: machinelearning/science-use-cases/hep-cnn.md
      - Current known issues: machinelearning/known_issues.md
    - Software Support Policy: policies/software-policy/index.md
  - Developing and Installing Software:
    - Compilers:
        - Compilers Overview: development/compilers/index.md
        - Base Compilers: development/compilers/base.md
        - Compiler Wrappers (recommended): development/compilers/wrappers.md
        - Compiling a Code: tutorials/playbooks/compiling.md # make into a real tutorial with supplied code to compile, mention /global/common/software
    - Build Tools:
        - Autoconf and Make: development/build-tools/autoconf-make.md
        - CMake: development/build-tools/cmake.md
        - Spack: development/build-tools/spack.md
    - Installing Software:
        - development/installing-software/index.md
    - Programming Models:
        - Programming Models Overview: development/programming-models/index.md
        - MPI:
          - Introduction to MPI: development/programming-models/mpi/index.md
          - Cray MPICH: development/programming-models/mpi/cray-mpich.md
          - Open MPI: development/programming-models/mpi/openmpi.md
          - Upstream MPICH: development/programming-models/mpi/mpich.md
        - OpenMP:
          - Introduction to OpenMP: development/programming-models/openmp/index.md
          - Compiling OpenMP Code: development/programming-models/openmp/compiling.md
          - Tools for OpenMP: development/programming-models/openmp/openmp-tools.md
        - OpenACC: development/programming-models/openacc/index.md
        - CUDA: development/programming-models/cuda/index.md
        - UPC: development/programming-models/upc.md
        - UPC++: development/programming-models/upcxx.md
        - Coarrays: development/programming-models/coarrays.md
        - SYCL: development/programming-models/sycl/index.md
        - Kokkos: development/programming-models/kokkos.md
        - HPX: development/programming-models/hpx.md
        - C++ parallel algorithms benchmark: development/programming-models/iso-cpp-parstl-benchmark/cppParSTL.md
    - Languages:
      - Julia: development/languages/julia.md
      - R: development/languages/r.md
      - IDL: development/languages/idl.md
      - Python:
          - Python Overview: development/languages/python/index.md
          - Intro to Python at NERSC: development/languages/python/python-intro.md
          - Using Python at NERSC: development/languages/python/nersc-python.md
          - Parallel Python: development/languages/python/parallel-python.md
          - Python in Shifter: development/languages/python/python-shifter.md
          - Profiling Python: development/languages/python/profiling-debugging-python.md
          - Python on AMD CPUs: development/languages/python/python-amd.md
          - Using Python on Perlmutter: development/languages/python/using-python-perlmutter.md
          - Porting Python to Perlmutter GPUs: development/languages/python/perlmutter-prep.md
          - FAQ and Troubleshooting: development/languages/python/faq-troubleshooting.md
    - Libraries:
      - development/libraries/index.md
      - FFTW: development/libraries/fftw/index.md
      - LAPACK: development/libraries/lapack/index.md
      - MKL: development/libraries/mkl/index.md
      - LibSci: development/libraries/libsci/index.md
      - HDF5: development/libraries/hdf5/index.md
      - NetCDF: development/libraries/netcdf/index.md
    - Performance:
      - Perlmutter Readiness: performance/readiness.md
      - Vectorization: performance/vectorization.md
      - Parallelism: performance/parallelism.md
      - Compiler Diagnostics: performance/compiler-diagnostics.md
      - I/O:
          - DVS: performance/io/dvs.md
          - performance/io/index.md
          - I/O Tuning: performance/io/library/index.md
          - Lustre: performance/io/lustre/index.md
          - File system in memory: performance/io/dev-shm.md
      - Portability: performance/portability.md
      - Variability: performance/variability.md
      - Network: performance/network.md
      - Performance Tools:
        - Performance Tools Overview: tools/performance/index.md
        - Codee: tools/performance/codee/index.md
        - CrayPat: tools/performance/craypat/index.md
        - Darshan:
          - tools/performance/darshan/index.md
          - DXT Explorer: tools/performance/darshan/dxt.md
          - Drishti: tools/performance/darshan/drishti.md
        - MAP: tools/performance/map/index.md
        - NVIDIA Profiling Tools: tools/performance/nvidiaproftools/index.md
        - Performance Reports: tools/performance/performancereports/index.md
        - Reveal: tools/performance/reveal/index.md
        - Roofline Performance Model: tools/performance/roofline/index.md
    - Debugging Tools:
      - Debugging Tools Overview: tools/debug/index.md
      - Compute Sanitizer: tools/debug/compute-sanitizer/index.md
      - CUDA-GDB: tools/debug/cuda-gdb/index.md
      - DDT: tools/debug/ddt/index.md
      - GDB: tools/debug/gdb/index.md
      - gdb4hpc and CCDB: tools/debug/gdb4hpc_ccdb/index.md
      - Sanitizers and sanitizers4hpc: tools/debug/sanitizers/index.md
      - STAT and ATP: tools/debug/stat_atp/index.md
      - TotalView: tools/debug/totalview/index.md
      - Valgrind and Valgrind4hpc: tools/debug/valgrind/index.md
  - Managing Data:
    - Unix File Permissions: filesystems/unix-file-permissions.md
    - PI Toolbox: filesystems/pitoolbox.md
    - Quotas: filesystems/quotas.md
    - Filesystems: 
      - Filesystems Overview: filesystems/index.md
      - Perlmutter scratch: filesystems/perlmutter-scratch.md
      - Community: filesystems/community.md
      - DnA: filesystems/dna.md
      - Archive (HPSS):
        - The HPSS Archive System: filesystems/archive.md
        - Storage Statistics: filesystems/archive-stats.md
      - Global Home: filesystems/global-home.md
      - Global Common: filesystems/global-common.md
      - Backups: filesystems/backups.md
    - Databases: services/databases.md
    - Moving Data:
      - File Transfer Software: services/index.md #clean up to only tx apps
      - Data Transfer Nodes: systems/dtn/index.md
      - Globus: services/globus.md
      - CVMFS: services/cvmfs.md
      - Frontier Cache: services/frontiercache.md
      - GridFTP: services/gridftp.md
      - Scp: services/scp.md
      - Bbcp: services/bbcp.md
      - Perlmutter Scratch: services/perlmutter-scratch.md
      - Files from Non-Users: services/nonuser-files.md
    - Data Policy: policies/data-policy/policy.md
  - Building Your Own Services:
    - Hosting Services with Spin:
      - Spin Overview: services/spin/index.md
      - Terminology: services/spin/terminology.md
      - Getting Started:
        - Local Development: services/spin/local.md
        - Running Your App in Spin: services/spin/running.md
        - Attaching Storage: services/spin/storage.md
        - Connecting to a Spin App: services/spin/connecting.md
      - Advanced Concepts:
        - Using the Rancher CLI: services/spin/cli.md
        - Development and Production environments: services/spin/envs.md
        - Managing Spin Apps with Helm: services/spin/helm.md
        - Migrating Apps from Docker Compose: services/spin/migrate.md
      - FAQ: services/spin/faq.md
      - Examples: services/spin/examples.md
    - API:
      - The Superfacility API: services/sfapi/index.md
      - Authentication: services/sfapi/authentication.md
      - Examples: services/sfapi/examples.md
      - Version: services/sfapi/versioning.md
    - Science Gateways: services/science-gateways.md
  - Managing Projects and Allocations:
    - Iris Guide for PIs and Project Managers: iris/iris-for-pis.md
    - ERCAP and Iris Guide for Allocation Managers: iris/iris-for-allocation-managers.md
  - Acronyms: help/acronyms.md

# Project Information
site_name: NERSC Documentation
site_description: NERSC Documentation
site_author: NERSC
site_dir: public
site_url: "https://docs.nersc.gov/"
repo_name: GitLab/NERSC/docs
repo_url: https://gitlab.com/NERSC/nersc.gitlab.io
edit_uri: blob/main/docs/

# Configuration
strict: true

theme:
  name: material
  custom_dir: nersc-theme
  favicon: assets/images/favicon.ico
  logo: assets/images/logo.png
  language: en
  features:
#    - navigation.indexes
    - content.action.edit

extra_javascript:
  - https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML
extra_css:
  - stylesheets/extra.css

extra:
  analytics:
    provider: google
    property: !ENV GA_TOKEN
  social:
    - icon: material/home-circle
      link: https://www.nersc.gov
    - icon: material/help-circle
      link: https://help.nersc.gov
    - icon: material/heart-pulse
      link: https://www.nersc.gov/live-status/motd/
    - icon: fontawesome/brands/github
      link: https://github.com/NERSC
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/NERSC
    - icon: fontawesome/brands/slack
      link: https://www.nersc.gov/users/NUG/nersc-users-slack/

# Extensions
markdown_extensions:
  - meta
  - attr_list
  - footnotes
  - admonition
  - codehilite:
      guess_lang: false
  - toc:
      permalink: true
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols:
      fractions: false
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.details
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - pymdownx.snippets

plugins:
  - search:
      separator: '[\s\-,:!=\[\]()"/]+|(?!\b)(?=[A-Z][a-z])|\.(?!\d)|&[lg]t;'
  - minify:
      minify_html: true
  - meta-descriptions
